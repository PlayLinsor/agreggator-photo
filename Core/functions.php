<?
/**  Photo Gallery 1.0
  *
  *	 14.02.16 Playlinsor@gmail.com
  *
*/

// Generation leftMenu links
function LeftMenuArray(){

	foreach ($_SESSION["sites"] as $key=>$value){
		$link = ""; 
		if ($_SESSION["CurrentSiteName"]==$key) $link = '<li><a href="?site='.$key.'" id="active">'.$key.'</a></li>'; 
			else $link = '<li><a href="?site='.$key.'">'.$key.'</a></li>';
		
		echo $link;
	}
	
}

// router links
function PrintLinks(){
	$url = $_SESSION["CurrentSite"];
	
	switch($_SESSION["type"]){
		case "gallery": 
			PrintGalleryImages($url,$_GET['n']); break;
			
		default:
			PrintLinksAll(genLinksGalleryAll($url,$_SESSION["page"]));
			
	}
}

// page Gallery, generation array links
function genLinksGalleryAll($url,$page){
	$count = $_SESSION["countImageOnPage"];
	$start = $page * $count - $count + 1;
	$end = $page * $count;
	
	for ($i=$start; $i<=$end; $i++){
		$num = $i;
		if ($i<1000 && $i>100)  $num = "0$i";
		if ($i<100 && $i>10) 	$num = "00$i";
		if ($i<10) 				$num = "000$i";
		
		$arr[$num] = $url.$num."/tn_00.jpg";
	}
	
	return $arr;
}

// page Gallery, print links from array
function PrintLinksAll($LinkArray){
	foreach ($LinkArray as $key=>$value){
		
		echo '<li><a href="?t=gal&n='.$key.'"><img src="Core/image.php?url='.$value.'" alt="" /></a><br /></li>';
	}
	
}

// Печать картинок от конкретной галлереи
function PrintGalleryImages($url,$gallery){
	$limit = $_SESSION["countImageOnGallery"];
	for ($i=0; $i<=$limit;$i++){
		$num = $i;
		
		if ($i<10) $num = "0$i"; 
		
		$value1 = $url.$gallery."/tn_".$num.".jpg"; // Превьюшка
		$value2 = $url.$gallery."/".$num.".jpg";
		
		echo '<li><a href="Core/image.php?url='.$value2.'" target="_blank"><img src="Core/image.php?url='.$value1.'" alt="" /></a><br /></li>';
	}
}


// Печать панели с линками
function PrintNavLinks(){
	if ($_SESSION["type"] != "gallery"){
		$current = $_SESSION["page"];
		$next = $current+1;
		$prev = $current-1;
		$limit = $_SESSION["sites"][$_SESSION['CurrentSiteName']]["limit"] / $_SESSION["countImageOnPage"] + 1;
		
		$text = '<a href="?page='.$prev.'">Previous</a><div><ul>';

		
		if ($current+20<$limit)	$linkLimit = $current + 14;
			else $linkLimit = $current - 3;
		
		if ($current>10) $startLimit = $current - 7;
			else $startLimit = 1;
			
		for ($i=$startLimit; $i<=$linkLimit;$i++){
			
			if ($i==$current) $text .= '<li><a href="#">['.$i.']</a></li>';
				else $text .= '<li><a href="?page='.$i.'">'.$i.'</a></li>';
			
		}
		
		$text .= "<li><a> . . . </a></li>";
		
		$trunc = floor($limit);
		for ($i=$trunc-2; $i<=$trunc;$i++){
			
			if ($i==$current) $text .= '<li><a href="#">['.$i.']</a></li>';
				else $text .= '<li><a href="?page='.$i.'">'.$i.'</a></li>';
			
		}
		
		
		$text .= '</ul></div><a href="?page='.$next.'">Next</a>';
		
	
		echo $text;
	}
	if ($_SESSION["type"] == "gallery"){
		$text = '<div class="center"><a href="'.$_SERVER['REQUEST_URI'].'&more=20">Попробывать грузонуть 20 фоток</a>';
		$text .='<a href="'.$_SERVER['REQUEST_URI'].'&more=25">Попробывать грузонуть 25 фоток</a></div>';
		echo $text;
	}
}













